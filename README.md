# docker-compose for microecodb.org

This docker-compose will start several containers:
  - an nginx reverse proxy
  - a mariadb database
  - ghost blog
  - background process to automatically sign certs using letsencrypt
